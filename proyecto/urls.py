from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.index, name="index"),
    path('formulario/', views.formulario, name="formulario"),
    path('formulario/crear/', views.crear, name="crear"),
    path('login',views.login,name="login"),
    path('cerrarsession',views.cerrar_session,name="cerrar_session"),
    path('login/iniciar',views.login_iniciar,name="iniciar"),
    path('crearMascotas/', views.crearMascotas, name="crearMascotas"),
    path('crearMascotas/crear_mascota/',views.crear_mascota, name="crear_mascota"),
    path('crearMascotas/modificar/<int:id>', views.modificar, name="modificar"),
    path('crearMascotas/administrarMascotas/<int:id>', views.administrarMascotas, name="administrarMascotas"),
    path('crearMascotas/eliminar/<int:id>', views.eliminar, name="eliminar"),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)