from django.db import models

# Create your models here.

class Formulario(models.Model):

    nombre = models.CharField(max_length=100)
    run = models.CharField(max_length=12)
    contrasenia = models.CharField(max_length=20)
    fechanaci = models.CharField(max_length=100)
    correo = models.CharField(max_length=20)
    celular = models.CharField(max_length=50)
    regiones = models.CharField(max_length=50)
    comunas = models.CharField(max_length=50)
    tipoCasa = models.CharField(max_length=10)
    
class Mascota(models.Model):

    nombre = models.CharField(max_length=100)
    raza = models.CharField(max_length=100)
    foto = models.ImageField(upload_to="fotos/")
    estado = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)
    