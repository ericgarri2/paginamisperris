from django.shortcuts import render
from django.http import HttpResponse
from .models import Formulario
from .models import Mascota
from django.shortcuts import redirect
#Importar usuario
from django.contrib.auth.models import User
#Sistema de autenticación
from django.contrib.auth import authenticate,logout, login as auth_login
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required





# Create your views here.

def index(request):
    usuario = request.session.get('usuario',None)
    return render(request,'index.html',{'name':'Registro de personas','usuario':usuario})
                
def formulario(request):
    return render(request, 'formulario.html',{})

def login(request):
    return render(request, 'login.html',{})

def mascotas(request):
    return render(request, 'mascotas.html',{})

def login_iniciar(request):
    nombre_usuario = request.POST.get('nombre_usuario','')
    contrasenia = request.POST.get('contrasenia','')
    user = authenticate(username=nombre_usuario, password=contrasenia)
    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect('index')
    else:
        return redirect('login')

def crear(request):
    nombre = request.POST.get('nombre', '')
    run = request.POST.get('run', '')
    contrasenia = request.POST.get('contrasenia', '')
    fechanaci = request.POST.get('fechanaci', '')
    correo = request.POST.get('correo', '')
    celular = request.POST.get('celular', '')
    regiones = request.POST.get('regiones', '')
    comunas = request.POST.get('comunas', '')
    tipoCasa = request.POST.get('tipoCasa', '')
    formulario = Formulario(nombre=nombre, run=run, contrasenia=contrasenia,fechanaci=fechanaci, correo=correo,
                            celular=celular, regiones=regiones, comunas=comunas, tipoCasa=tipoCasa)
    formulario.save()
    user = User.objects.create_user(username=nombre,password=contrasenia,
    email=correo,first_name=nombre)
    user.save()
    return HttpResponse("Usuario Creado con exito")

@login_required(login_url='login')
@staff_member_required(login_url='login')
def crear_mascota(request):
    nombre = request.POST.get('nombre', '')
    raza = request.POST.get('raza', '')
    foto = request.FILES.get('foto','')
    estado = request.POST.get('estado', '')
    descripcion = request.POST.get('descripcion', '') 
    mascota = Mascota(nombre=nombre, raza=raza, foto=foto, estado=estado, descripcion=descripcion)
    mascota.save()
    return redirect('crearMascotas')

@login_required(login_url='login')
@staff_member_required(login_url='login')
def crearMascotas(request):
    usuario = request.session.get('usuario',None)
    return render(request, 'crearMascotas.html',{'mascotas': Mascota.objects.all(),'usuario':usuario})

@login_required(login_url='login')
@staff_member_required(login_url='login')
def modificar(request,id):
    nombre = request.POST.get('nombre', '')
    raza = request.POST.get('raza', '')
    foto = request.FILES.get('foto','')
    estado = request.POST.get('estado', '')
    descripcion = request.POST.get('descripcion', '') 
    mascota = Mascota.objects.get(pk=id)
    mascota.nombre = nombre
    mascota.raza = raza
    mascota.foto = foto
    mascota.estado = estado
    mascota.descripcion = descripcion
    mascota.save()
    return redirect('crearMascotas')

@login_required(login_url='login')
@staff_member_required(login_url='login')
def administrarMascotas(request,id):
    mascota = Mascota.objects.get(id=id)
    print(mascota)
    return render(request,'administrarMascotas.html',{'mascota':mascota})


@login_required(login_url='login')
@staff_member_required(login_url='login')
def eliminar(request,id):
    mascota = Mascota.objects.get(pk=id)
    mascota.delete()
    return redirect('crearMascotas')

@login_required(login_url='login')
def cerrar_session(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')

